package br.com.netshoes.webservice.cep.repository;

import br.com.netshoes.webservice.cep.model.Cep;

public interface CepRepository {
	Cep getByNumero(String numero);
}
