package br.com.netshoes.webservice.cep.service;

import org.springframework.stereotype.Service;

import br.com.netshoes.webservice.cep.model.Cep;

@Service
public interface CepService {
	
	Cep get(String numero);
}
