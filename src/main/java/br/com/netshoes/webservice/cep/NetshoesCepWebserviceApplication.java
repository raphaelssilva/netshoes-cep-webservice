package br.com.netshoes.webservice.cep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NetshoesCepWebserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(NetshoesCepWebserviceApplication.class, args);
    }
}
