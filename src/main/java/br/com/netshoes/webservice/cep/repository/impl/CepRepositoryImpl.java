package br.com.netshoes.webservice.cep.repository.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import br.com.netshoes.webservice.cep.model.Cep;
import br.com.netshoes.webservice.cep.repository.CepRepository;
@Repository
public class CepRepositoryImpl implements CepRepository{
	Map<String, Cep> ceps = null;
	
	
	@Override
	public Cep getByNumero(String numero) {
		this.popularCeps();
		return ceps.get(numero);
	}
	
	private void popularCeps(){
		if(ceps==null||ceps.isEmpty()){
			ceps = new HashMap<String, Cep>();
			
			ceps.put("12345678", new Cep("12346578", "Rua a", "Agui Branca", "Vitoria", "ES"));
			ceps.put("12346500", new Cep("12346500", "Rua X", "Castelo branco", "Vitoria", "ES"));
			ceps.put("121232", new Cep("121232", "Rua Nilton", "Joquei Clube", "Vitoria", "ES"));
			ceps.put("982345", new Cep("982345", "Rua P", "Bela Auroro", "Vitoria", "ES"));
			ceps.put("29141280", new Cep("29141280", "Rua Padre Pires", "Boa Sorte", "Cariacica", "ES"));
			ceps.put("13565090", new Cep("13565090", "Rua Ray Wesley", "Joquei Clube", "São Carlos", "SP"));
			ceps.put("23456789", new Cep("23456789", "Rua a", "Itaparica", "Vitoria", "SP"));
			ceps.put("1234580", new Cep("1234580", "Rua E", "Jardineiro", "São Paulo", "SP"));
			
		}
	}

}
